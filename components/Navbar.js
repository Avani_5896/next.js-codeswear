import React from 'react'
import Link from 'next/link'
import Image from 'next/image'
import {
  AiOutlineShoppingCart,
  AiFillCloseCircle,
  AiFillPlusCircle,
  AiFillMinusCircle,
} from 'react-icons/ai';
import { BsFillBagCheckFill } from 'react-icons/bs';
import { MdAccountCircle } from 'react-icons/md';
import { useRouter } from 'next/router'
import { useRef, useEffect, useState } from 'react'

const Navbar = ({ logout, user, cart, addToCart, removeFromCart, clearCart, subTotal }) => {
  const [dropdown, setDropdown] = useState(false)
  const [sidebar, setSidebar] = useState(false)

  const router = useRouter();

  useEffect(() => {
  //  Object.keys(cart).length !== 0 && setSidebar(true)
   let exmpted = ['checkout', 'order', 'orders', 'myaccount', 'admin']
   if(exmpted.includes(router.pathname)){
     setSidebar(false);
   }
  }, [])
  

  const toggleCart = () => {
    setSidebar(!sidebar)
    // if (ref.current.classList.contains('translate-x-full')) {
    //   ref.current.classList.remove('translate-x-full')
    //   ref.current.classList.add('translate-x-0')
    // }
    // else if (!ref.current.classList.contains('translate-x-full')) {
    //   ref.current.classList.remove('translate-x-0')
    //   ref.current.classList.add('translate-x-full')
    // }
  }
  const ref = useRef()
  return (
    <div className='sticky top-0 bg-white z-10 flex felx-col md:flex-row md:justify-start justify-center items-center py-2 shadow-xl'>
      <div className="logo ml-auto md:mx-5">
        <Link href={'/'}><a><Image src="/logo.webp" alt="" width={200} height={40} /></a></Link>
      </div>
      <div className="nav">
        <ul className='flex items-center space-x-6 font-bold md:text-sm'>
          <Link href={'/tshirts'}><a><li className=" hover:text-pink-500">Tshirts</li></a></Link>
          <Link href={'/hoodies'}><a><li className=" hover:text-pink-500">Hoodies</li></a></Link>
          <Link href={'/stickers'}><a><li className=" hover:text-pink-500">Stickers</li></a></Link>
          <Link href={'/mugs'}><a><li className=" hover:text-pink-500">Mugs</li></a></Link>
        </ul>
      </div>
      <div className="flex cursor-pointer items-center cart absolute right-0 top-4 mx-5">
        <span onMouseOver={()=>setDropdown(true)} onMouseLeave={()=>setDropdown(false)}>
          {dropdown && <div onMouseOver={()=>setDropdown(true)} onMouseLeave={()=>setDropdown(false)} className="absolute right-8 top-6 rounded-md px-5 py-4 w-32 bg-white shadow-lg border">
            <div>
              <ul>
                <Link href={'/myaccount'}><a><li className='py-1 hover:text-pink-700 text-sm font-semibold'>My Account</li></a></Link>
                <Link href={'/orders'}><a><li className='py-1 hover:text-pink-700 text-sm font-semibold'>Orders</li></a></Link>
                <a onClick={logout}><li className='py-1 hover:text-pink-700 text-sm font-semibold'>Log Out</li></a>
              </ul>
            </div>
          </div>}
          {user.value && <MdAccountCircle className='text-xl md:text-2xl mx-2' />}
        </span>
        {!user.value && <Link href={'/login'}><a><button className='bg-pink-600 px-2 py-1 rounded-md text-sm text-white mx-2'>Login</button></a>
        </Link>}
        
        {user.value && <AiOutlineShoppingCart onClick={toggleCart} className='text-xl md:text-2xl' />}
      </div>
      <div ref={ref} className={`w-72 h-[100vh] sideCart overflow-y-scroll absolute top-0 bg-pink-100 px-8  py-10 transform transition-transform  ${sidebar ? 'right-0' : '-right-96'}`}>
        <h2 className='font-bold text-xl text-center'>Shooping Cart</h2>
        <span onClick={toggleCart} className="absolute top-5 right-2 cursor-pointer text-2xl">
          <AiFillCloseCircle />
        </span>
        <ol className='list-decimal font-semibold'>
          {Object.keys(cart).length == 0 &&
            <div className='my-4 text-base font-semibold'>Your Cart is Empty</div>}
          {Object.keys(cart).map((k) => {
            return <li key={k}>
              <div className="item flex my-5">
                <div className='w-2/3 font-semibold'>{cart[k].name}({cart[k].size}/{cart[k].variant})</div>
                <div className='w-1/3 font-semibold flex items-center justify-center text-lg'><AiFillMinusCircle onClick={() => { removeFromCart(k, 1, cart[k].price, cart[k].size, cart[k].variant) }} className='cursor-pointer text-pink-500' /><span className='mx-2 text-sm'>{cart[k].qty}</span><AiFillPlusCircle onClick={() => { addToCart(k, 1, cart[k].price, cart[k].size, cart[k].variant) }} className='cursor-pointer text-pink-500' /></div>
              </div>
            </li>
          })}
        </ol>
        <div className="total font-bold my-3">SubTotal: {subTotal}</div>
        <div className="flex">
          <Link href={'/checkout'}><button disabled={Object.keys(cart).length === 0}className="disabled:bg-pink-300 flex mr-2 text-white bg-pink-500 border-0 py-2 px-2 focus:outline-none hover:bg-pink-600 rounded text-sm"><BsFillBagCheckFill className='m-1' />Checkout</button></Link>
          <button disabled={Object.keys(cart).length === 0} onClick={clearCart} className="disabled:bg-pink-300 flex mr-2 text-white bg-pink-500 border-0 py-2 px-2 focus:outline-none hover:bg-pink-600 rounded text-sm">Clear Cart</button>
        </div>

      </div>
    </div>
  )
}

export default Navbar