import React from 'react'
import { useRef } from react
import { AiFillCloseCircle } from 'react-icons/ai';

const Sidebar = () => {
    const toggleCart = () => {
        if (ref.current.classList.contains('translate-x-full')) {
            ref.current.classList.remove('translate-x-full')
            ref.current.classList.add('translate-x-0')
        }
        else if (!ref.current.classList.contains('translate-x-0')) {
            ref.current.classList.remove('translate-x-0')
            ref.current.classList.add('translate-x-full')
        }
    }
    const ref = useRef(second)
    return (
        <div ref={ref} className='sidecart absolute top-0 right-0 bg-pink-100 p-10 transition-transform translate-x-full transform'>
            <h2 className='font-bold text-xl'>Shooping Cart</h2>
            <span onClick={toggleCart} className="absolute top-5 right-2 cursor-pointer text-2xl"><AiFillCloseCircle /></span>
            <ol>
                <li>
                    <span>Tshirt- Wear the code</span>
                </li>
            </ol>
        </div>
    )
}

export default Sidebar