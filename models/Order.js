const mongoose = require('mongoose')

const OrderSchema = new mongoose.Schema({
    orderId: {
        type: String,
        required: true
    },
    transcationId: {
        type: String,
        default:"",
    },
    paymentInfo: {
        type: String,
        default: '',
    },
    email: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    products: {
        type: Object,
        required: true
    },
    address: {
        type: String,
        required: true,
    },
    city: {
        type: String,
        required: true,
    },
    state: {
        type: String,
        required: true,
    },
    pincode: {
        type: String,
        required: true,
    },
    phone: {
        type: String,
        required: true,
    },
    amount: {
        type: String,
        required: true,
    },
    status: {
        type: String,
        default: 'initiated',
        required: true,
    },
    deliveryStatus: {
        type: String,
        default: 'pending',
        required: true,
    }
},
    { timestamps: true }
)

mongoose.models = {}
export default mongoose.model("order", OrderSchema);