import { Html, Head, Main, NextScript } from 'next/document'

export default function Document() {
  return (
    <Html className='overflow-x-hiiden'>
      <Head />
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}