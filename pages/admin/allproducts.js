import React from 'react'
import { ThemeProvider } from "@mui/material/styles";
import FullLayout from "../../src/layouts/FullLayout";
import theme from "../../src/theme/theme";
import { Grid } from "@mui/material";
import AllProducts from "../../src/components/dashboard/AllProducts";
import mongoose from "mongoose";
import Product from '../../models/Product';

const Allproducts = ({products}) => {
    return (
        <ThemeProvider theme={theme}>
            <style jsx global>{`
        footer {
          display: none;
        }
      `}</style>
            <FullLayout>
                <Grid container spacing={0}>
                    <Grid item xs={12} lg={12}>
                        <AllProducts products={products} />
                    </Grid>
                </Grid>
            </FullLayout>
        </ThemeProvider>
    )
}

export async function getServerSideProps(context) {
    if (!mongoose.connections[0].readyState) {
      await mongoose.connect(process.env.MONGO_URI)
    }
  
    let error=null;
    let products = await Product.find()
  
    if (products == null) {
      return {
        props: {
          error: 404,
        },
      }
    }
    return {
      props: {
        error: error,
        products: JSON.parse(JSON.stringify(products)),
  
      }, // will be passed to the page component as props
    }
  }

export default Allproducts