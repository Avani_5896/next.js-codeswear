import jsonwebtoken from "jsonwebtoken"
import connectDb from "../../middleware/mongoose"
import User from "../../models/User"

const handler = async (req, res) => {
    if (req.method == 'POST') {
        let token = req.body.token
        let user = jsonwebtoken.verify(token, process.env.JWT_SECRET)
        let dbuser = await User.findOne({ email: user.email });
        const {name, email, address, pincode, phone, city, state} = dbuser
        res.status(200).json({ name, email, address, pincode, phone, city, state })

    } else {
        res.status(400).json({ error: error })
    }

}

export default connectDb(handler);