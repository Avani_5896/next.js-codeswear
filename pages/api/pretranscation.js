const https = require('https');
const PaytmChecksum = require('paytmchecksum');
import connectDb from "../../middleware/mongoose"
import Order from "../../models/Order"
import Product from "../../models/Product"
import pincodes from "../../pincodes.json"

const handler = async (req, res) => {
    if (req.method == 'POST') {

        // check if the pincode is servicable
        if(!Object.keys(pincodes).includes(req.body.pincode)){
            if (req.body.subTotal <= 0) {
                res.status(200).json({ success: false, "error": "The pincode you have entered is not serviceable", cartClear:false })
                return
            }
        }

        // check if the card is tempered with
        let prodcut, sumTotal = 0;
        let cart = req.body.cart;
        if (req.body.subTotal <= 0) {
            res.status(200).json({ success: false, "error": "Cart Empty!Please build your cart & Try again", cartClear:false })
            return
        }
        for (let item in cart) {
            sumTotal += cart[item].price * cart[item].qty
            prodcut = await Product.findOne({ slug: item })
            // check if the cart items are out of stock 
            if (prodcut.availableqty < cart[item].qty) {
                res.status(200).json({ success: false, "error": "Some items in your cart went out of stock, Please Try again", cartClear:true })
            }
            if (prodcut.price != cart[item].price) {
                res.status(200).json({ success: false, "error": "The price of some items in your cart has changed, Please Try again", cartClear:true })
                return
            }
        }
        if (sumTotal != req.body.subTotal) {
            res.status(200).json({ success: false, "error": "The price of some items in your cart has changed, Please Try again", cartClear:true })
            return
        }

        // check if the details are valid
        if (req.body.phone.length !== 10 || !Number.isInteger(Number(req.body.phone))) {
            res.status(200).json({ success: false, "error": "Please Enter your 10 digits phone number", cartClear:false })
            return
        }
        if (req.body.pincode.length !== 6 || !Number.isInteger(Number(req.body.pincode))) {
            res.status(200).json({ success: false, "error": "Please Enter your 6 digits pincode", cartClear:false })
            return
        }

        // Initiate an order corresponding to this orderid
        let order = new Order({
            name: req.body.name,
            email: req.body.email,
            orderId: req.body.oid,
            address: req.body.address,
            city: req.body.city,
            state: req.body.state,
            pincode: req.body.pincode,
            phone: req.body.phone,
            amount: req.body.subTotal,
            products: req.body.cart
        })
        await order.save();
        console.log(order);
        // Insert an entry to the order table with status as pending
        var paytmParams = {};

        paytmParams.body = {
            "requestType": "Payment",
            "mid": process.env.NEXT_PUBLIC_PAYTM_MID,
            "websiteName": "YOUR_WEBSITE_NAME",
            "orderId": req.body.oid,
            "callbackUrl": `${process.env.NEXT_PUBLIC_HOST}/api/posttranscation`,
            "txnAmount": {
                "value": req.body.subTotal,
                "currency": "INR",
            },
            "userInfo": {
                "custId": req.body.email,
            },
        };

        /*
        * Generate checksum by parameters we have in body
        * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys 
        */
        const checksum = await PaytmChecksum.generateSignature(JSON.stringify(paytmParams.body), process.env.NEXT_PUBLIC_PAYTM_MKEY)

        paytmParams.head = {
            "signature": checksum
        };

        var post_data = JSON.stringify(paytmParams);

        const requestAsync = async () => {
            return new Promise((resolve, reject) => {
                var options = {

                    /* for Staging */
                    hostname: 'securegw-stage.paytm.in',

                    /* for Production */
                    // hostname: 'securegw.paytm.in',

                    port: 443,
                    path: `/theia/api/v1/initiateTransaction?mid=${process.env.NEXT_PUBLIC_PAYTM_MID}&orderId=${req.body.oid}`,
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Content-Length': post_data.length
                    }
                };

                var response = "";
                var post_req = https.request(options, function (post_res) {
                    post_res.on('data', function (chunk) {
                        response += chunk;
                    });

                    post_res.on('end', function () {
                        // console.log('Response: ', response);
                        let ress = JSON.parse(response).body
                        ress.success = true;
                        ress.cartClear = false;
                        // resolve(ress.body);
                        resolve(ress);
                    });
                });

                post_req.write(post_data);
                post_req.end();
            })
        }

        let myreq = await requestAsync();
        res.status(200).json(myreq);
    }
}

export default connectDb(handler);
