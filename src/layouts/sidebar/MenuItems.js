const Menuitems = [
  {
    title: "Dashbaord",
    icon: "home",
    href: "/admin",
  },
  {
    title: "Add Product",
    icon: "plus-circle",
    href: "/admin/add",
  },
  {
    title: "View Products",
    icon: "eye",
    href: "/admin/allproducts",
  },
  {
    title: "Image Uploader",
    icon: "upload",
    href: "/admin/imageuploader",
  },
  {
    title: "Order",
    icon: "shopping-cart",
    href: "/admin/allorders",
  },
  // {
  //   title: "Images",
  //   icon: "image",
  //   href: "/image",
  // },
  // {
  //   title: "Pagination",
  //   icon: "user",
  //   href: "/pagination",
  // },
  // {
  //   title: "Tables",
  //   icon: "grid",
  //   href: "/table",
  // },
];

export default Menuitems;
